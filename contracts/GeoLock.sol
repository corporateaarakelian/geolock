pragma solidity 0.7.6;

import "./helpers/Claimable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC777/ERC777.sol";
import "@openzeppelin/contracts/introspection/IERC1820Registry.sol";

contract GeoLock is Claimable {
    using SafeMath for uint;

    uint256 public constant MAX_LOCK_AMOUNT = 300000000000000000000000;
    uint256 public constant MULTIPLIER = 3;

    IERC1820Registry private _erc1820 =
        IERC1820Registry(0x1820a4B7618BdE71Dce8cdc73aAB6C95905faD24); // See EIP1820
    bytes32 private constant TOKENS_SENDER_INTERFACE_HASH =
        keccak256("ERC777TokensSender"); // See EIP777
    bytes32 private constant TOKENS_RECIPIENT_INTERFACE_HASH =
        keccak256("ERC777TokensRecipient"); // See EIP777

    address public geoToken;

    mapping(address => uint256) public lockedAssets;
    mapping(address => bool) public whitelist;

    constructor (address _geoTokenAddress, address[] memory _whitelist) public {
        require(_geoTokenAddress != address(0), "Token is zero address");
        geoToken = _geoTokenAddress;

        for (uint256 i = 0; i < _whitelist.length; i++) {
            whitelist[_whitelist[i]] = true;
        }
         // Register as a oken receiver
        _erc1820.setInterfaceImplementer(
            address(this),
            TOKENS_RECIPIENT_INTERFACE_HASH,
            address(this)
        );
        // Register as a token sender
        _erc1820.setInterfaceImplementer(
            address(this),
            TOKENS_SENDER_INTERFACE_HASH,
            address(this)
        );
    }

    function tokensToSend(
        address, /*operator*/
        address from,
        address, /*to*/
        uint256, /*amount*/
        bytes calldata, /*userData*/
        bytes calldata /*operatorData*/
    ) external view {
        require(
            msg.sender == geoToken,
            "Can only be called by the GeoDB GeoTokens contract"
        );
        require(from == address(this), "Sender is not SelfContract");
    }

    function tokensReceived(
        address operator,
        address, /*from*/
        address, /*to*/
        uint256, /*amount*/
        bytes calldata, /*userData*/
        bytes calldata /*operatorData*/
    ) external view {
        require(
            msg.sender == geoToken,
            "Can only receive GeoDB GeoTokens"
        );
        require(
            operator == address(this),
            "Can only receive tokens from this contract"
        );
    }

    modifier onlyWhitelisted() {
        require(whitelist[msg.sender], "Not whitelisted");
        _;
    }

    function addToWhitelist(address[] memory _whitelist) external onlyContractOwner() {
        for (uint256 i = 0; i < _whitelist.length; i++) {
              whitelist[_whitelist[i]] = true;
          }
    }

    function lock(uint256 _amount) external onlyWhitelisted() {
        uint256 _userAsset = lockedAssets[msg.sender];
        require(_userAsset.add(_amount) <= MAX_LOCK_AMOUNT, "Total lock amount exceeds MAX_LOCK_AMOUNT");

        ERC777(geoToken).transferFrom(msg.sender, address(this), _amount);

        lockedAssets[msg.sender] = _userAsset.add(_amount);
    }

    function unlock(address _user) external onlyContractOwner() {
        uint256 lockedAmount = lockedAssets[_user];
        lockedAssets[_user] = 0;
        ERC777(geoToken).transfer(_user, lockedAmount);
    }

    function burnForUser(address _user) external onlyContractOwner() {
        uint256 lockedAmount = lockedAssets[_user];
        lockedAssets[_user] = 0;
        ERC777(geoToken).burn(lockedAmount, "");
    }

    function burnAll() external onlyContractOwner() {
        ERC777(geoToken).burn(ERC777(geoToken).balanceOf(address(this)), "");
    }

    function getDividedAmount(address _user) external view returns(uint256) {
        return lockedAssets[_user].div(MULTIPLIER);
    }
}
