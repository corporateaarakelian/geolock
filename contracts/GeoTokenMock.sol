pragma solidity 0.7.6;

import "@openzeppelin/contracts/token/ERC777/ERC777.sol";

contract GeoTokenMock is ERC777 {
    constructor () public ERC777("geo", "geo", new address[](0)) {
        _mint(msg.sender, 1000000000000000000000000000, '', '');
    }
}
