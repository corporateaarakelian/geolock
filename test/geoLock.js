const GeoLock = artifacts.require('GeoLock');
const GeoToken = artifacts.require('GeoTokenMock');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');

contract('GeoLock', async (accounts) => {
  const reverter = new Reverter(web3);

  let geoLock;
  let geoToken;

  const SOMEBODY = accounts[1];
  const NOBODY = accounts[2];

  before('setup', async () => {
    geoToken = await GeoToken.new();
    geoLock = await GeoLock.new(geoToken.address, [SOMEBODY]);

    await reverter.snapshot();
  });

  afterEach('revert', reverter.revert);

  describe('lock()', async () => {
    it('should be possible to lock by whitelisted', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), amount.toString());

      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), 0);
      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
    });

    it('should not be possible to lock by not whitelisted', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(NOBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: NOBODY});

      await truffleAssert.reverts(geoLock.lock(amount, {from: NOBODY}), 'Not whitelisted');

      assert.equal(bn(await geoToken.balanceOf(NOBODY)).toString(), amount.toString());
    });

    it('should not be possible to lock by whitelisted if exceeds 300k', async () => {
      const amount = bn('300000000000000000000001');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});

      await truffleAssert.reverts(geoLock.lock(amount, {from: SOMEBODY}));

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), amount.toString());
    });
  });

  describe('unlock()', async () => {
    it('should be possible to unlock by owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), 0);

      await geoLock.unlock(SOMEBODY);

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), amount.toString());
    });

    it('should not be possible to unlock by not owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), 0);

      await truffleAssert.reverts(geoLock.unlock(SOMEBODY, {from: SOMEBODY}),
        'Not a contract owner');

      assert.equal(bn(await geoToken.balanceOf(SOMEBODY)).toString(), 0);
    });
  });

  describe('addToWhitelist()', async () => {
    it('should be possible to addToWhitelist by owner', async () => {
      assert.equal(await geoLock.whitelist(accounts[3]), false);
      assert.equal(await geoLock.whitelist(accounts[4]), false);

      await geoLock.addToWhitelist([accounts[3], accounts[4]]);

      assert.equal(await geoLock.whitelist(accounts[3]), true);
      assert.equal(await geoLock.whitelist(accounts[4]), true);
    });

    it('should not be possible to addToWhitelist by not owner', async () => {
      assert.equal(await geoLock.whitelist(accounts[3]), false);
      assert.equal(await geoLock.whitelist(accounts[4]), false);

      await truffleAssert.reverts(geoLock.addToWhitelist([accounts[3], accounts[4]], {from: SOMEBODY}),
        'Not a contract owner');

      assert.equal(await geoLock.whitelist(accounts[3]), false);
      assert.equal(await geoLock.whitelist(accounts[4]), false);
    });
  });

  describe('burnForUser()', async () => {
    it('should be possible to burnForUser by owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.toString());

      await geoLock.burnForUser(SOMEBODY);

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), 0);
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), 0);
    });

    it('should not be possible to burnForUser by not owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.toString());

      await truffleAssert.reverts(geoLock.burnForUser(SOMEBODY, {from: SOMEBODY}), 'Not a contract owner');

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.toString());
    });
  });

  describe('burnAll()', async () => {
    it('should be possible to burnAll by owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      await geoToken.transfer(NOBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: NOBODY});
      await geoLock.addToWhitelist([NOBODY]);
      await geoLock.lock(amount, {from: NOBODY});

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoLock.lockedAssets(NOBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.times(2).toString());

      await geoLock.burnAll();

      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), 0);
    });

    it('should not be possible to burnAll by not owner', async () => {
      const amount = bn('1000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      await geoToken.transfer(NOBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: NOBODY});
      await geoLock.addToWhitelist([NOBODY]);
      await geoLock.lock(amount, {from: NOBODY});

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoLock.lockedAssets(NOBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.times(2).toString());

      await truffleAssert.reverts(geoLock.burnAll({from: SOMEBODY}), 'Not a contract owner');

      assert.equal(bn(await geoLock.lockedAssets(SOMEBODY)).toString(), amount.toString());
      assert.equal(bn(await geoLock.lockedAssets(NOBODY)).toString(), amount.toString());
      assert.equal(bn(await geoToken.balanceOf(geoLock.address)).toString(), amount.times(2).toString());
    });
  });

  describe('getDividedAmount()', async () => {
    it('should be possible to getDividedAmount and get correct amount', async () => {
      const amount = bn('3000000000000000000000');

      await geoToken.transfer(SOMEBODY, amount);
      await geoToken.approve(geoLock.address, amount, {from: SOMEBODY});
      await geoLock.lock(amount, {from: SOMEBODY});

      assert.equal(bn(await geoLock.getDividedAmount(SOMEBODY)).toString(), amount.div(3).toString());
    });
  });
});

function bn(number) {
  return new BigNumber(number);
}
